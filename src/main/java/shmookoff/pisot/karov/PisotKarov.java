package shmookoff.pisot.karov;

import net.fabricmc.api.ModInitializer;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.util.Identifier;
import net.minecraft.util.registry.Registry;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class PisotKarov implements ModInitializer {
	// This logger is used to write text to the console and the log file.
	// It is considered best practice to use your mod id as the logger's name.
	// That way, it's clear which mod wrote info, warnings, and errors.
	public static final Logger LOGGER = LoggerFactory.getLogger("pisotkarov");

	public static final Item ADIN_KAROV = new Item(new Item.Settings().group(ItemGroup.MISC));
	public static final Item PYAT_KAROV = new Item(new Item.Settings().group(ItemGroup.MISC));
	public static final Item DESIT_KAROV = new Item(new Item.Settings().group(ItemGroup.MISC));
	public static final Item PISYAT_KAROV = new Item(new Item.Settings().group(ItemGroup.MISC));
	public static final Item STO_KAROV = new Item(new Item.Settings().group(ItemGroup.MISC));
	public static final Item PISOT_KAROV = new Item(new Item.Settings().group(ItemGroup.MISC));

	@Override
	public void onInitialize() {
		Registry.register(Registry.ITEM, new Identifier("pisotkarov", "adin_karov"), ADIN_KAROV);
		Registry.register(Registry.ITEM, new Identifier("pisotkarov", "pyat_karov"), PYAT_KAROV);
		Registry.register(Registry.ITEM, new Identifier("pisotkarov", "desit_karov"), DESIT_KAROV);
		Registry.register(Registry.ITEM, new Identifier("pisotkarov", "pisyat_karov"), PISYAT_KAROV);
		Registry.register(Registry.ITEM, new Identifier("pisotkarov", "sto_karov"), STO_KAROV);
		Registry.register(Registry.ITEM, new Identifier("pisotkarov", "pisot_karov"), PISOT_KAROV);
	}
}
